<?php

namespace Modules\Admin\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Modules\Admin\Repositories\VersionRepositary as VersionRepo;
use Datatables;
use Illuminate\Http\Request;
use Route;
use Toastr;
use Helper;
use Elasticsearch;

class VersionController extends Controller
{

    /**
     * This function is used to display version listing.
     * 
     * @param void
     * @return view
     * @author LY
     */
    public function index()
    {
        return view('admin::version.index');
    }

    /**
     * This function is used to get the list of versions.
     * 
     * @param void
     * @return json $versions
     * @author LY
     */
    public function getVersionList(Request $request)
    {
        $versionRepo = new VersionRepo();

        //Get filters
        $filters = $request->all();

        $versions = Datatables::of($versionRepo->getVersions($filters))
                        ->editColumn('created_at', '{!! Helper::showDateTime($created_at) !!}')
                        ->addColumn('action', function ($version) {
                            $actions = '';
                            //show action field only in case of backend database
                            if ($version->status_id == 2 || $version->status_id == 3) {
                                $actions = '<form method="POST" action="' . Route('admin.version.live') . '" class="make_live_form" name="' . $version->db_name . '" >' .
                                        '<input type="hidden" name="databaseId" value="' . $version->id . '">' .
                                        '<input type="hidden" name="_token" value="' . csrf_token() . '">' .
                                        '<button type="submit" class="btn btn-success">Make Live</button>' .
                                        '</form>';
                            }

                            return $actions;
                        })->make(true);

        return $versions;
    }

    /**
     * This function is used to make the version live.
     * 
     * @param void
     * @return json $result
     * @author LY 
     */
    public function setVersionLive(Request $request)
    {
        $versionRepo = new VersionRepo();

        //Get request data
        $data = $request->all();

        //Set backend version as live if present in request
        if (isset($data['databaseId']) && !empty($data['databaseId'])) {

            //Set backend version as live and replicate backend database for further changes
            $result = $versionRepo->setLiveVersion($data['databaseId']);

            try {
                new \Modules\Frontend\Helpers\ElasticSearchHelper();
            } catch (\Elasticsearch\Common\Exceptions\NoNodesAvailableException $ex) {
            }

            if ($result) {
                Toastr::success('Data is live now. Live version: ' . 'bti_v' . $data['databaseId']);
            } else {
                Toastr::error('Some Error occured.');
            }
        } else {
            Toastr::error('Wrong request.');
        }

        return Redirect()->back();
    }
}
