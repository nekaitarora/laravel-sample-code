<?php 
namespace Modules\Admin\Repositories;

use DB;
use Modules\Admin\Entities\Model\Version\Version;
use Modules\Admin\Entities\Model\Version\VersionStatus;
use Artisan;
use Config;
use Carbon\Carbon;

class VersionRepositary
{

    /**
     * This function is used to get list of versions.
     * 
     * @param void
     * @return object $versions
     * @author LY 
     */
    public function getVersions($filters = array())
    {
        $versions = Version::with('VersionStatus')
            ->whereNULL('deleted_at')
            ->where(function ($query) use ($filters) {
                if (isset($filters['start_date']) && isset($filters['end_date']) && strtotime($filters['start_date']) && strtotime($filters['end_date'])) {
                    $query->whereBetween('created_at', array( date('Y-m-d 00:00:00', strtotime($filters['start_date'])), date('Y-m-d 23:59:59', strtotime($filters['end_date']))));
                }
            });

        return $versions;
    }

    /**
     * This function is used to make backend version live.
     * 
     * @param int $versionId
     * @return object $versions
     * @author LY 
     */
    public function setLiveVersion($versionId)
    {
        //Replicate database.
        $newDatabase = $this->replicateDatabase($versionId);
        
        //Proced if new database created
        if ($newDatabase) {
            
            //Get version details
            $versionDetail = Version::find($versionId);
          
            //Get live version and set it to archived
            $liveVersionDetail = $this->getLiveVersion();
            if (!empty($liveVersionDetail)) {
                $liveVersionDetail->status_id = 3;
                $liveVersionDetail->save();
            }

            //Set backend version status to live
            $versionDetail->status_id = 1;
            $versionDetail->save();

            //Get version name
            $dbName = explode('_', $newDatabase);
            $versionName = isset($dbName[1])? $dbName[1] : '';
           
            //Set new backend version Databse
            $version = new Version();
            $version->db_name = $newDatabase;
            $version->version = strtoupper($versionName);
            $version->status_id = 2;
            $version->save();
            
            Version::where('db_name', '>', $versionDetail->db_name)->where('db_name', '<', $newDatabase)->delete();
           
            return true;
        } else {
            return false;
        }
    }

    /**
     * This funtion is used to get the backend version.
     * 
     * @param void
     * @param object $editableVersion
     * @author LY
     */
    public function getBackendVersion()
    {

        //Get Editable version
        $editableVersion = Version::with('VersionStatus')
            ->where('status_id', 2)
            ->where('version', '!=', 'V0')
            ->first();

        return $editableVersion;
    }

    /**
     * This funtion is used to get the backend version.
     * 
     * @param void
     * @param object $editableVersion
     * @author LY
     */
    public function getLiveVersion()
    {
        //Get Editable version
        $editableVersion = Version::with('VersionStatus')
            ->where('status_id', 1)
            ->where('version', '!=', 'V0')
            ->first();

        return $editableVersion;
    }

    /**
     * This function is used to replicate database.
     * 
     * @param int $databaseId
     * @return string $newDatabaseName 
     * @author LY
     */
    public function replicateDatabase($databaseId)
    {

        //Get version details
        $versionDetail = Version::find($databaseId);

        try {

            //Get Master DB name
            $masterDb = Config::get('database.connections.master.database');

            //Get next AI for database name
            $aiSql = "SELECT AUTO_INCREMENT FROM information_schema.tables
                    WHERE table_name = 'version'
                    AND table_schema = '" . $masterDb . "';";

            $verionTableDetail = DB::select($aiSql);
            $nextID = isset($verionTableDetail[0]->AUTO_INCREMENT) ? $verionTableDetail[0]->AUTO_INCREMENT : 1;

            //New database name
            $newDbName = 'bti_v' . ($nextID - 1);

            //Create new database for backend
            $sql = "Create Database IF NOT EXISTS $newDbName";
            DB::statement($sql);

            //Dumping previous data to new database
            Artisan::call('copy:database', [
                    'source' => $versionDetail->db_name,
                    'target' => $newDbName,
            ]);

            //If any error creating database than delete new created database.
            $datbase = DB::select('SHOW DATABASES LIKE "'.$newDbName.'"');
            if (!empty($datbase)) {
                return $newDbName;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }
}
